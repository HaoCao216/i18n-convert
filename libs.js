const XLSX = require('xlsx');
const fs = require('fs');

const SHEET_KEY = "ORIGINAL - DO NOT CHANGE!!!";
const SHEET_NAME = "translation";

const getFile = () => {
    const content = XLSX.readFile('./file1.xlsx', { cellDates: false });
    return content;
};

const getSheet = () => {
    const file = getFile();
    let worksheet = file.Sheets[SHEET_NAME];
    let headers = {};
    let data = [];
    for (z in worksheet) {
        if (z[0] === '!') continue;
        const col = z.substring(0, 1);
        const row = parseInt(z.substring(1));
        const value = worksheet[z].v;
        if (row == 1) {
            headers[col] = value;
            continue;
        }

        if (!data[row]) data[row] = {};
        data[row][headers[col]] = value;
    }
    data.shift();
    data.shift();
    const newHeader = Object.values(headers);
    newHeader.forEach((key) => {
        if (key != SHEET_KEY) {
            const language = key;
            const languageData = {};
            data.map((item) => {
                const dataKey = item[SHEET_KEY]
                languageData[dataKey] = item[language];
            })
            const exportLanguage = {
                [language.toLowerCase()]: languageData
            }
            write(language,exportLanguage);
        }
    });
}

const write = (name, data) => {
    fs.writeFile(`languages/${name.toLowerCase()}.json`,JSON.stringify(data), 'utf8', (err, data) => {
        if (err) {
            console.log(err)
        }
    });
};

module.exports = getSheet();